# terraform-provider-hcloud-ci

[gh: terraform-provider-hcloud](https://github.com/hetznercloud/terraform-provider-hcloud)

You can find the binary of **terraform provider for hetzner cloud**
from here: [terraform-provider-hcloud-ci](https://gitlab.com/terraform-provider/terraform-provider-hcloud-ci/-/jobs/artifacts/main/raw/bin/terraform-provider-hcloud?job=run-build)

## Install

```bash
VERSION=1.50.0

mkdir -pv ~/.local/share/terraform/plugins/registry.terraform.io/hashicorp/hcloud/${VERSION}/linux_amd64/
cd        ~/.local/share/terraform/plugins/registry.terraform.io/hashicorp/hcloud/${VERSION}/linux_amd64/

curl -sLo terraform-provider-hcloud https://gitlab.com/terraform-provider/terraform-provider-hcloud-ci/-/jobs/artifacts/main/raw/bin/terraform-provider-hcloud?job=run-build

chmod +x terraform-provider-hcloud

```
